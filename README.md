#### TypeScript客户端核心SDK 说明

官网: http://www.echatim.cn

文档: https://www.jianshu.com/u/5ffeb01f41d9

核心SDK使用Typescript开发, 使用js-conditional-compile-loader插件条件编译出微信小程序端, Web网页端使用的js sdk

打包出微信小程序使用的sdk:
```bash
yarn build:wxlib # 将会在 dist 目录生成echatim-sdk.js 文件.
```


打包出Web网页端使用的sdk:
```bash
yarn build:weblib # 将会在 dist 目录生成echatim-sdk.js 文件.
```


打包出ReactNative端使用的sdk:
```bash
yarn build:rnlib # 将会在 dist 目录生成echatim-lastest.tar.gz Typescript打包文件.
```



打包出uniapp端使用的sdk:
```bash
yarn build:unilib # dist 目录生成echatim-sdk.js 文件.
```





#### 版本记录:

##### v1.10 - 2020-10-13

* sdk支持自动登录, 非自动登录.(ok)

* 用户/游客注册, 登录，登出功能.

* 暴露 http,socket.io 常规调用接口.(ok)

* 暴露 socket.io 常规监听接口.(ok)

* 客户端支持设置客户端离线时推送(方式: 1.短信; 2.邮件; 3. 苹果ios推送; 4. 极光推送; ) (仅专业版)

* 客户端支持同一用户多个客户端登录, 同时支持消息漫游 (仅专业版)

* 工具类: 1.获取当前客户端的ip 地址; 2.客户端唯一性id(支持平台: 1.浏览器)

##### v 1.03 - 2020-10-12

* 整理错误码, 与client-cpp-core 项目保持一致.

* 调整sdk初始化完成逻辑，设置为在用户登录完成之后.


##### v 1.02 - 2020-06-28

* 加入微信小程序平台支持, 包括通讯, 文件上传等功能.


##### v 1.01 - 2020-05-24
* 第一版源码发布
