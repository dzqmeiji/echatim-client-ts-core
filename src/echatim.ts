import { echatIMSDK } from "./echatim/core/sdk/EChatIMSDK";
import Logger from "./echatim/log/Logger";
import EChatIMApis from "./echatim/core/apis/EChatIMApis";
import WebToolkit from "./echatim/common/WebToolkit";
import version from './version'

let hadShowLog = false;
/**
 *  echatIMSDK module package entry
 * */
if(typeof window !== 'undefined'){
    window['im'] = echatIMSDK;
    window['im_webtoolkit'] = WebToolkit;
    window['imsdk_version'] = version;
    if(!hadShowLog){
        console.log(` == using echatim:${version.version} for platform:${version.platform} == `);
        console.log(` 欢迎使用E聊(www.echat.work),技术交流QQ群:471688937`);
        hadShowLog = true;
    }
}
if(typeof global !== 'undefined'){
    global['im'] = echatIMSDK;
    global['im_webtoolkit'] = WebToolkit;
    global['imsdk_version'] = version;

    if(!hadShowLog){
        console.log(` == using echatim:${version.version} for platform:${version.platform} == `);
        console.log(` 欢迎使用E聊(www.echat.work),技术交流QQ群:471688937`);
        hadShowLog = true;
    }
}
// @ts-ignore
if(typeof uni !== 'undefined'){
    // @ts-ignore
    uni['im'] = echatIMSDK;
    // @ts-ignore
    uni['im_webtoolkit'] = WebToolkit;
    // @ts-ignore
    uni['imsdk_version'] = version;

    if(!hadShowLog){
        console.log(` == using echatim:${version.version} for platform:${version.platform} == `);
        console.log(` 欢迎使用E聊(www.echat.work),技术交流QQ群:471688937`);
        hadShowLog = true;
    }
}



export {
    echatIMSDK as im,
    EChatIMApis as apis,
    Logger as log,
    WebToolkit as webtoolkit,
    version
};

