import PluploadClient from "./support/PluploadClient";
import BrowserXMLHttpClient from "./support/BrowserXMLHttpClient";
import ReactNativeFetchClient from "./support/ReactNativeFetchClient";
import ReactNativeXMLHttpClient from "./support/ReactNativeXMLHttpClient";
import WxClient from "./support/WxClient";
import UniAppClient from "./support/UniAppClient";

// 文件服务器配置(阿里云OSS, 本地文件服务器等), 所用的文件上传客户端工具配置(Fetch, XMLHttp, Plupload 等)
export class FileServerConfig {
    use:string = "";
    client:string = "";
    baseUrl:string = "";
    version:string = "v1"; // API使用的版本(默认为v1)
}


export class FileUploadConfig {
    domId:string = "";
    maxFileSize:number = 10240; // 上传文件的上限(mb)
    allowSuffix:Array<string> = []; // 允许上传文件的后缀名, 不设置则为全部文件可以上传.
    type: string = ""; // 上传文件类型
}

export interface FileUploadListener {
    beforeUploadCallback: (info: FileInfo) => FileInfo|string;
    progressCallback: (info: FileInfo) => void;
    uploadedCallback: (info: FileInfo) => void;
    errorCallback: (info: FileInfo|undefined, error: UploadError) => void;
}

export class FileInfo {
    id:string = ""; // 文件ID
    name:string = ""; // 文件名称
    originFileName:string = "";
    size:number = 0; // 文件大小(kb)
    uploadPercent:number = 0; // 文件上传进度
    type: string = "";
    url: string = "";
}

export class UploadError {
    code:number = 0; // 上传错误码
    msg:string = ""; // 上传错误信息
}

// 文件服务器接口
export interface IFileServerClient {
    init(config:FileUploadConfig, listeners: FileUploadListener|undefined):void;
    // 主动上传文件(异步执行), 返回上传文件的id
    upload(path:string):string;
}


export class FileServerClientFactory {
    public static create(config: FileServerConfig, sdkKey:string):IFileServerClient|any{
        if('aliyun' === config.use || 'local' === config.use){
/*IFTRUE_WEBAPP*/
            if('rn-fetch' === config.client){
                return new ReactNativeFetchClient(config, sdkKey); // 已不建议使用
            }
            else if('rn-xmlhttp' === config.client){
                return new ReactNativeXMLHttpClient(config, sdkKey);
            }
            else if('xmlhttp' === config.client){
                return new BrowserXMLHttpClient(config, sdkKey);
            }
            else if(!config.client || 'plupload' === config.client){
                return new PluploadClient(config, sdkKey);
            }
/*FITRUE_WEBAPP*/

/*IFTRUE_RNAPP*/
            if('rn-fetch' === config.client){
                return new ReactNativeFetchClient(config, sdkKey); // 已不建议使用
            }
            else if('rn-xmlhttp' === config.client){
                return new ReactNativeXMLHttpClient(config, sdkKey);
            }
            else if('xmlhttp' === config.client){
                return new BrowserXMLHttpClient(config, sdkKey);
            }
            else if(!config.client || 'plupload' === config.client){
                return new PluploadClient(config, sdkKey);
            }
/*FITRUE_RNAPP*/

/*IFTRUE_WXAPP*/
            if('wx' === config.client){
                return new WxClient(config, sdkKey);
            }
            else {
                throw new Error(`not support wechat app platform`);
            }
/*FITRUE_WXAPP*/


/*IFTRUE_UNIAPP*/
            if('uniapp' === config.client){
                return new UniAppClient(config, sdkKey);
            }
            else {
                throw new Error(`not support wechat app platform`);
            }
/*FITRUE_UNIAPP*/
        }
        else {
            throw new Error(`not support file-server type:${config.use}`);
        }
    }
}
