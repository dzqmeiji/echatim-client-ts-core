import { FileInfo, FileUploadConfig, FileUploadListener } from "../FileServerClient";
import UUID from "../../../common/UUID";
import Beans from "../../../common/utils";
import Logger from "../../../log/Logger";
import AbstractClient from "./AbstractClient";
import { sdk_error_message } from "../../sdk/Errors";


class FetchResponse {
    type: string = '';
    status: number = 0;
    ok: boolean = false;
}

/**
 *  Note:
 *  微信小程序文件上传客户端.
 *  适用SDK平台: 微信小程序
 *  配置方式
 sdkConfig.fileServerConfig = {
        use: 'aliyun',
        // use: 'local',
        client: 'wx',
        baseUrl: 'http://192.168.0.100:8082',
        version: 'v1',
    };
 *
 * */

// 文件服务器Fetch客户端
export default class WxClient extends AbstractClient{
    init(uploadConfig: FileUploadConfig, listeners: FileUploadListener | undefined): void {
        const self = this;
        self.uploadConfig = uploadConfig;
        if(listeners !== undefined){
            this.beforeUploadCallback = listeners.beforeUploadCallback;
            this.progressCallback = listeners.progressCallback;
            this.uploadedCallback = listeners.uploadedCallback;
            this.errorCallback = listeners.errorCallback;
        }
    }

    public upload(path:string):string{
        const self = this;
        // 触发用户额外自定义的 beforeUploadCallback 事件
        let fileInfo = new FileInfo();
        if(!path){
            this.setErrorCallback(sdk_error_message[1101].code, sdk_error_message[1101].msg);
            return "";
        }
        fileInfo.type = path.substring(path.lastIndexOf('.')+1);
        const filename = new Date().getTime() + "." + fileInfo.type;
        fileInfo.id = UUID.gen();
        fileInfo.name = filename;
        fileInfo.originFileName = filename;
        fileInfo.size = 0; // 服务器计算?

        if(typeof self.beforeUploadCallback === 'function'){
            const info = self.beforeUploadCallback(fileInfo);
            if(typeof info === 'string' && info === 'cancel'){
                return "";
            }
            if(info){
                self.fileInfo = info;
            }
            else {
                self.fileInfo = fileInfo;
            }
        }
        else {
            self.fileInfo = fileInfo;
        }

        self.buildFormParams(fileInfo, function (host, formData) {
            self.applyLocalServerUploadDo(path, host, formData)
        });


        return fileInfo.id;
    }


    // 本地上传的细节处理 self: fileclient 实例
    private applyLocalServerUploadDo(filePath, host, formParams:any):void {
        // const fetch = Fetch.getFetchToolkit();
        const self = this;
        if(!self.signInfo || !self.fileInfo){
            return;
        }
        let uploadUrl = '';
        if(this.serverConfig.use === 'local'){
            uploadUrl = (self.serverConfig.baseUrl + '/' + self.serverConfig.version + '/' + 'common/file/upload');
        }
        else if(this.serverConfig.use === 'aliyun'){
            uploadUrl = host;
        }
        let options:any = {};
        // options.body = formData;
        options.method = 'POST';
        options.headers = {
            'Content-Type': 'multipart/form-data',
        };
        Logger.info(`ready fetch upload:${uploadUrl}  with options:${Beans.json(options)}  fileInfo:${Beans.json(self.fileInfo)}`);

        if(self.serverConfig.use === 'aliyun'){
            //@ts-ignore
            wx.uploadFile({
                url: uploadUrl,
                filePath: filePath,
                name: 'file',
                header: options.headers,
                formData: formParams,
                success: function (res) {
                    if (res.statusCode === 200) {
                        const response = JSON.parse(res.data);
                        self.handleAliyunServerResponse(response as FetchResponse);
                    }
                    else {
                        self.setErrorCallback(sdk_error_message[1126].code, sdk_error_message[1126].msg + " detail:"+JSON.stringify(res));
                    }
                },
                fail: function (e) {
                    Logger.trace(e);
                    self.setErrorCallback(sdk_error_message[1126].code, sdk_error_message[1126].msg + " detail:"+ e);
                },
                complete: function () {
                }
            })

        }
        else if(self.serverConfig.use === 'local'){
            //@ts-ignore
            wx.uploadFile({
                url: uploadUrl,
                filePath: filePath,
                name: 'file',
                header: options.headers,
                formData: formParams,
                success: function (res) {
                    if (res.statusCode === 200) {
                        const response = JSON.parse(res.data);
                        self.handleLocalServerResponse(response as any);
                    } else {
                        self.setErrorCallback(sdk_error_message[1126].code, sdk_error_message[1126].msg + " detail:"+JSON.stringify(res));
                    }
                },
                fail: function (e) {
                    Logger.trace(e);
                    self.setErrorCallback(sdk_error_message[1126].code, sdk_error_message[1126].msg + " detail:"+ e);
                },
                complete: function () {
                }
            })
        }
    };

    private handleLocalServerResponse(res:any){
        const self = this;
        if(!res.data || !res.data.url){
            self.setErrorCallback(sdk_error_message[1103].code, sdk_error_message[1103].msg + " detail:" + res.msg);
            return;
        }
        if(!self.fileInfo){
            return;
        }
        self.fileInfo.url = res.data.url;
        // 上传完成
        if(typeof self.uploadedCallback === 'function'){
            self.fileInfo.uploadPercent = 100;
            self.uploadedCallback(self.fileInfo);
        }
    }

    private handleAliyunServerResponse(response:FetchResponse){
        const self = this;

        function aliyunResponseDetailLog(response:FetchResponse) {
            // @ts-ignore
            return `response: ${Beans.json(response)}`
        }
        if (response.status === 200) {
            // 阿里云上传成功返回值放在头部内, body没有返回值
            Logger.info(`上传文件成功, 服务器返回:${aliyunResponseDetailLog(response)}`);
            if(!self.fileInfo){
                return;
            }
            self.sendSyncUploadCallback({
                filename: self.signInfo.dir,
                originFileName: self.fileInfo.originFileName,
                fileType: self.fileInfo.type
            }).then((resp=>{
                // 同步回调发送完成
                if(typeof self.uploadedCallback === 'function' && self.fileInfo !== undefined && self.signInfo !== undefined){
                    self.fileInfo.uploadPercent = 100;
                    self.fileInfo.url = self.signInfo.host + '/' + self.signInfo.dir;
                    self.uploadedCallback(self.fileInfo);
                }
            })).catch(e =>{
                Logger.trace(e)
                self.setErrorCallback(sdk_error_message[1107].code, sdk_error_message[1107].msg + " detail:" + e);
            });
        }
        else {
            self.setErrorCallback(sdk_error_message[1126].code,
              // @ts-ignore
              sdk_error_message[1126].msg + ` detail:${aliyunResponseDetailLog(response)}`);
        }
    }


}
