import { FileUploadConfig, FileUploadListener } from "../FileServerClient";
import Beans from "../../../common/utils";
import Logger from "../../../log/Logger";
import AbstractClient from "./AbstractClient";
import { sdk_error_message } from "../../sdk/Errors";

// XMLHttpClient 抽象类
export default class AbstractXMLHttpClient extends AbstractClient{
    init(uploadConfig: FileUploadConfig, listeners: FileUploadListener | undefined): void {
        const self = this;
        self.uploadConfig = uploadConfig;
        if(listeners !== undefined){
            this.beforeUploadCallback = listeners.beforeUploadCallback;
            this.progressCallback = listeners.progressCallback;
            this.uploadedCallback = listeners.uploadedCallback;
            this.errorCallback = listeners.errorCallback;
        }

    }

    protected handleLocalServerResponse(xhr:XMLHttpRequest){
        const self = this;
        if (xhr.status === 200) {
            if(xhr.responseText === ''){
                self.setErrorCallback(sdk_error_message[1103].code, sdk_error_message[1103].msg);
                return;
            }

            const res = Beans.bean(xhr.responseText);
            if(!res.data || !res.data.url){
                self.setErrorCallback(sdk_error_message[1103].code, sdk_error_message[1103].msg + " return:" + res.msg);
                return;
            }
            if(!self.fileInfo){
                return;
            }
            self.fileInfo.url = res.data.url;
            // 上传完成
            if(typeof self.uploadedCallback === 'function'){
                self.fileInfo.uploadPercent = 100;
                self.uploadedCallback(self.fileInfo);
            }
        }
        else {
            self.setErrorCallback(sdk_error_message[1103].code,
                // @ts-ignore
              sdk_error_message[1103].msg + ` return: responseHeader:${xhr.responseHeaders !== undefined ? Beans.json(xhr.responseHeaders) : xhr.getAllResponseHeaders()} responseBody:${xhr.responseText}`);
        }
    }

    protected handleAliyunServerResponse(xhr:XMLHttpRequest){
        const self = this;

        function aliyunResponseDetailLog(xhr:XMLHttpRequest) {
            // @ts-ignore
            return `responseHeader: ${ typeof xhr.responseHeaders !== 'undefined' ? Beans.json(xhr.responseHeaders) : xhr.getAllResponseHeaders()} responseBody:${xhr.responseText}`
        }

        if (xhr.status === 200) {
            // 阿里云上传成功返回值放在头部内, body没有返回值
            // Logger.info(`上传文件成功, 服务器返回:${aliyunResponseDetailLog(xhr)}`);
            if(!self.fileInfo){
                return;
            }
            self.sendSyncUploadCallback({
                filename: self.signInfo.dir,
                originFileName: self.fileInfo.originFileName,
                fileType: self.fileInfo.type
            }).then((resp=>{
                // 同步回调发送完成
                if(typeof self.uploadedCallback === 'function' && self.fileInfo !== undefined && self.signInfo !== undefined){
                    self.fileInfo.uploadPercent = 100;
                    self.fileInfo.url = self.signInfo.host + '/' + self.signInfo.dir;
                    self.uploadedCallback(self.fileInfo);
                }
            })).catch(e =>{
                Logger.trace(e);
                self.setErrorCallback(sdk_error_message[1107].code, sdk_error_message[1107].msg + " detail:" + e);
            });
        }
        else {
            self.setErrorCallback(sdk_error_message[1103].code,
                // @ts-ignore
              sdk_error_message[1103].msg + ` return:${aliyunResponseDetailLog(xhr)}`);
        }
    }


}
