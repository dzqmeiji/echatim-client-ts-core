
/**
 *
 *  sdk error code distribution:
 *       sdk: 1000 ~ 1100
 *       fileclient: 1100 ~ 1200
 *       socketio 1200 ~ 1300
 *
 * */

export const sdk_error_message = {
  1000:{code:1000, msg:"sdk error"},

  1100:{code:1100, msg:"fileclient error."},
  1101:{code:1101, msg:"file path is empty."},
  1102:{code:1102, msg:"could not get filename."},
  1103:{code:1103, msg:"file server return failed."},
  1104:{code:1104, msg:"Not support fileServer type."},
  1105:{code:1105, msg:"curl upload file to fileserver failed."},
  1106:{code:1106, msg:"aliyun oss async callback failed."},
  1107:{code:1107, msg:"aliyun oss sync callback failed."},

  1120:{code:1120, msg:"fetch upload sign failed."},
  1121:{code:1121, msg:"only support upload one file."},
  1122:{code:1122, msg:"dom not exist."},
  1123:{code:1123, msg:"dom relate type error."},
  1124:{code:1124, msg:"dom file onchange failed."},

  1125:{code:1125, msg:"plupload server error."},
  1126:{code:1126, msg:"server error."},

  1200:{code:1200, msg:"socketio error"},
};
