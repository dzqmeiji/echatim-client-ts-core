/**
 *  EChatIMSDK .
 *  config:{
 *      host: easyIM 服务器
 *      key: sdk key
 *      secret: sdk secret.
 *      apiTransport: API 通讯方式(SSL)
 *  }
 *
 * */
import { ISocket, socket } from "./Socket";
import Logger from "../../log/Logger";
import UserLoginForm from "../../model/form/UserLoginForm";
import ApiResponse from "../apis/ApiResponse";
import EChatIMApis from "../apis/EChatIMApis";
import { jwtCache } from "../cache/jwt";
import UserFriendListDTO from "../../model/dto/UserFriendListDTO";
import HistorySessionDTO from "../../model/dto/HistorySessionDTO";
import UserBlackForbidListDTO from "../../model/dto/UserBlackForbidListDTO";
import RoomListDTO from "../../model/dto/RoomListDTO";
import MessageReceiveDTO from "../../model/dto/MessageReceiveDTO";
import { Topic } from "../../common/Topic";
// import RoomListForm from "../../model/form/RoomListForm";
import UserBlackForbidListForm from "../../model/form/UserBlackForbidListForm";
import UserFriendListForm from "../../model/form/UserFriendListForm";
import HistoryListSessionForm from "../../model/form/HistoryListSessionForm";
import RoomListJoinForm from "../../model/form/RoomListJoinForm";
import UserListDTO from "../../model/dto/UserListDTO";
import UserListForm from "../../model/form/UserListForm";
import ClientUserOnlineForm from "../../model/form/ClientUserOnlineForm";
import { FileServerClientFactory, FileServerConfig, IFileServerClient } from "../fileclient/FileServerClient";
import HttpApi from "../apis/HttpApi";
import SocketIOApi from "../apis/SocketIOApi";
import SocketStatusManage from "./SocketStatusManage";
import Beans from "../../common/utils";
import Encrypt from "../../common/Encrypt";
import ProtocolMessage from "../../model/protocol/ProtocolMessage";


export interface IEChatIMSDKListener {
    onConnected():void; // 客户端已连接
    onDisConnected():void; // 客户端失去连接
    onUserInfo(userInfo: UserListDTO); // 获取用户信息
    onMessageReceive(message: MessageReceiveDTO); // 用户接收到消息
    onEventReceive(message: MessageReceiveDTO); // 用户接收到消息
    onSessionList(list:Array<HistorySessionDTO>):void; // 初始化加载会话列表
    onFriendList(list:Array<UserFriendListDTO>):void; // 初始化加载好友列表
    onBlacklistForbidList(list:Array<UserBlackForbidListDTO>):void; // 初始化加载黑白单, 禁言列表
    onRoomList(list:Array<RoomListDTO>):void; // 初始化加载群组列表

    onSocketConnectEvent(connectOption:any, status:string, data:any):void; // socket 连接事件

    onError(e:any):void;// 异常回调
}



export class EChatIMSDKConfig {
    host:string = "";
    socketPort:number = 9091;
    httpPort:number = 80;
    key:string = "";
    secret:string = "";
    apiTransport:string = "";
    loginAuid:string = "";
    loginToken:string = "";
    fileServerConfig:FileServerConfig|any = null;
    listeners: IEChatIMSDKListener|any = null;
    autoLogin:boolean = true; // 是否自动登录(默认: 开启)
    heartbeat:boolean = true; // 是否开启应用层心跳包(默认: 开启)
    debug:boolean = true; // 是否开启调试(默认: 开启)
    locale:string = ""; // 国际化多语言支持(en-US, zh-CN)
    einfo:any={enable:false, k:'',t:''};
}



export class EChatIMSDKState extends EChatIMSDKConfig{
    socketConnected: boolean = false;
}

let firstInit: boolean = false;// 第一次初始化
let firstMonitorSocket: boolean = false;// 第一次监听socket
export class EChatIMSDK {


    /**
     *  EChatIMSDK .
     *  config:{
     *      host: easyIM 服务器
     *      key: sdk key
     *      secret: sdk secret.
     *      apiTransport: API 通讯方式(SSL)
     *
     *  }
     *
     *  export apiSocket, http.
     * */

    config: EChatIMSDKConfig = new EChatIMSDKConfig();
    socketConnected: boolean = false;
    logined: boolean = false;
    apis: EChatIMApis|any = EChatIMApis;
    heartbeatInterval: any = null;
    constructor(){
        this.apis = EChatIMApis;
    }

    init(config:EChatIMSDKConfig, callback: (sdk:EChatIMSDK) => void): EChatIMSDK {
        const self = this;
        self.config.host = config.host ? config.host : 'localhost';
        self.config.socketPort = config.socketPort ? config.socketPort : 80;
        self.config.httpPort = config.httpPort ? config.httpPort : 80;
        self.config.apiTransport = config.apiTransport ? config.apiTransport : '';
        self.config.key = config.key;
        self.config.secret = config.secret;
        self.config.loginToken = config.loginToken;
        self.config.loginAuid = config.loginAuid;
        self.config.listeners = config.listeners; // 应用配置的监听器
        self.config.fileServerConfig = config.fileServerConfig;
        if(config.autoLogin !== undefined){
            self.config.autoLogin = config.autoLogin;
        }
        if(config.heartbeat !== undefined){
            self.config.heartbeat = config.heartbeat;
        }
        if(config.debug !== undefined){
            self.config.debug = config.debug;
            Logger.debug = self.config.debug; // 设置log开关
        }
        if(config.locale !== undefined){
            self.config.locale = config.locale;
        }
        // TODO: verify sdk with key,secret.
        Logger.info(`ready to connect:${(config.apiTransport === 'SSL' ? 'https://' : 'http://') + self.config.host + ':' + self.config.socketPort}`);
        socket.connect((config.apiTransport === 'SSL' ? 'https://' : 'http://') + self.config.host + ':' + self.config.socketPort);
        if(!firstMonitorSocket){
            firstMonitorSocket = true;
            // 加入socket 连接事件
            if(typeof self.config.listeners.onSocketConnectEvent === 'function'){
                const socketManage = new SocketStatusManage(socket['socket']);
                socketManage.addListener((option, status, data)=>{
                    self.config.listeners.onSocketConnectEvent(option, status, data);
                })
            }
        }
        socket.listen('connect', function(msg: any, connectOption:any) {
            Logger.info('apiSocket.io connected!');
            self.socketConnected = true;
            if(!firstInit && !self.config.autoLogin){
                callback(self);
                firstInit = true;
            }
            EChatIMApis.systemInfo.call({}).then((res:ApiResponse<string>)=>{
                if(res.isSucceed() && res.data.hasOwnProperty('einfo')){
                    self.config.einfo = Beans.to(res.data.einfo);
                }
            }).catch((e:any)=>{
                Logger.trace(e);
            });
            // 连接时自动登录
            if(self.config.autoLogin){
                self.autoLogin(function () {
                    self.logined = true;
                    callback(self); // 仅用户登录成功时, 初始化才完成.
                    if(self.config.listeners){
                        self.initConfigSocketioMessageListener();
                        self.initConfigImListener();
                    }
                });
            }
            // sdk 初始化时不自动登录, 需要外部应用自行管理用户的登录状态.
            else {
                // 仅初始化socketio 相关的消息监听即可
                self.initConfigSocketioMessageListener();
            }
            if(self.config.listeners && typeof self.config.listeners.onConnected === 'function'){
                self.config.listeners.onConnected();
            }
        });
        socket.listen('disconnect', function(msg: any) {
            Logger.info('apiSocket.io disconnected!');
            self.socketConnected = false;
            self.logined = false;
            if(self.config.listeners && typeof self.config.listeners.onDisConnected === 'function'){
                self.config.listeners.onDisConnected();
            }
        });
        if(self.heartbeatInterval == null && self.config.heartbeat === true){
            self.heartbeatInterval = setInterval(()=>{
                if(!self.socketConnected || !jwtCache.jwt){
                    return;
                }
                EChatIMApis.heartbeat.call({}).then((res:ApiResponse<string>)=>{
                }).catch((e:any)=>{
                    if(typeof self.config.listeners.onError === 'function'){
                        self.config.listeners.onError(e);
                    }
                });
            }, 60*1000); // 应用层心跳包间隔(1分钟1次)
        }

        return this;
    }

    public newFileClient():IFileServerClient {
        // 文件服务器
        if(this.config.fileServerConfig){
            return FileServerClientFactory.create(this.config.fileServerConfig, this.config.key);
        }
        else {
            throw new Error('没有fileServerConfig配置, 无法创建file client 实例');
        }
    }

    // when socket connect with the server, auto login
    public autoLogin(finishHandler:()=>void): void {
        if(!this.config.key || !this.config.loginAuid || !this.config.loginToken){
            return;
        }
        const self = this;
        const form = new UserLoginForm();
        form.auid = this.config.loginAuid;
        form.token = this.config.loginToken;
        form.appKey = this.config.key;
        EChatIMApis.login.call(form).then((res:ApiResponse<string>)=>{
            Logger.info('auto Login ok');
            if(res.isSucceed()){
                if(!res.data){
                    throw Error('server not return jwt yet.');
                }
                jwtCache.jwt = res.data!.toString();
                if(typeof finishHandler === 'function'){
                    finishHandler();
                }
            }
            else {
                throw Error('get jwt failed.');
            }
        }).catch((e:any)=>{
            Logger.error('apiSocket.io disconnected!');
            Logger.trace(e);
            this.getSocket().disConnect();
            if(typeof self.config.listeners.onError === 'function'){
                self.config.listeners.onError(e);
            }
        });
    }

    public getState(): EChatIMSDKState | any{
        if(!this.config){
            throw Error('EChatIMSDK not config yet.');
        }
        return {
            socketConnected: this.socketConnected,
            ...this.config
        }
    }



    private initConfigSocketioMessageListener(){
        if(!this.config.listeners){
            return;
        }
        const self = this;
        if(typeof this.config.listeners.onMessageReceive === 'function' || typeof this.config.listeners.onEventReceive === 'function'){
            self.getSocket().removeAllListener(Topic.APP_DOWNSTREAM_MESSAGE.topic_name); // 防止重连时重复添加监听
            self.getSocket().listen(Topic.APP_DOWNSTREAM_MESSAGE.topic_name, function(message){
                if(self.config.einfo.enable && message.hasOwnProperty('t') && message.hasOwnProperty('c') && message.t && message.c){
                    const content = Encrypt.decrypt(message.c, self.config.einfo.k);
                    message = Beans.bean(content) as ProtocolMessage;
                }
                if(message.method === Topic.APP_DOWNSTREAM_MESSAGE.METHOD.SEND_MESSAGE_TO_CLIENT){
                    if(typeof self.config.listeners.onMessageReceive === 'function'){
                        self.config.listeners.onMessageReceive(message);
                    }
                }
                else if(message.method === Topic.APP_DOWNSTREAM_MESSAGE.METHOD.SEND_EVENT_TO_CLIENT){
                    if(typeof self.config.listeners.onEventReceive === 'function'){
                        self.config.listeners.onEventReceive(message);
                    }
                }
            });
        }
    }

    private initConfigImListener(){
        if(!this.config.listeners){
            return;
        }
        const self = this;
        if(typeof this.config.listeners.onUserInfo === 'function'){
            const userListForm = new UserListForm();
            userListForm.auids = [self.config.loginAuid];
            EChatIMApis.userList.call(userListForm).then((response)=>{
                if(response.data.length > 0){
                    self.config.listeners.onUserInfo(response.data[0]);
                }
                else {
                    throw new Error("无法找到登录用户IM信息.");
                }
            }).catch((e)=>{
                Logger.trace(e);
            })
        }
        if(typeof this.config.listeners.onSessionList === 'function'){
            const historyListSessionForm = new HistoryListSessionForm();
            historyListSessionForm.auid = self.config.loginAuid;
            historyListSessionForm.startTimestamp = 0;
            historyListSessionForm.endTimestamp = new Date().getTime();
            EChatIMApis.historyListSession.call(historyListSessionForm).then((response)=>{
                const form = new ClientUserOnlineForm();
                const sessions = (response.data as Array<any>);
                form.auids = sessions.map(function (v) {
                    return v.toTarget;
                });
                if(form.auids.length === 0){
                    self.config.listeners.onSessionList([]);
                    return;
                }
                EChatIMApis.userOnline.call(form).then(resp=>{
                    const userOnlineMapByAuid = {};
                    (resp.data as Array<any>).forEach(vv => userOnlineMapByAuid[vv.auid] = vv);
                    sessions.forEach(session => {
                        session.online = userOnlineMapByAuid.hasOwnProperty(session.toTarget) ? userOnlineMapByAuid[session.toTarget].online : 0
                    });
                    self.config.listeners.onSessionList(sessions);
                }).catch((e)=>{
                    Logger.trace(e);
                    if(typeof self.config.listeners.onError === 'function'){
                        self.config.listeners.onError(e);
                    }
                })

                // self.config.listeners.onSessionList(response.data);
            }).catch((e)=>{
                Logger.trace(e);
            })
        }

        if(typeof this.config.listeners.onFriendList === 'function'){
            const userFriendListForm = new UserFriendListForm();
            userFriendListForm.auid = self.config.loginAuid;
            EChatIMApis.listFriends.call(userFriendListForm).then((response)=>{
                const form = new ClientUserOnlineForm();
                const friends = (response.data as Array<any>);
                form.auids = friends.map(function (v) {
                    return v.auid;
                });
                if(form.auids.length === 0){
                    return;
                }
                EChatIMApis.userOnline.call(form).then(resp=>{
                    const userOnlineMapByAuid = {};
                    (resp.data as Array<any>).forEach(vv => userOnlineMapByAuid[vv.auid] = vv);
                    friends.forEach(friend => {
                        friend.online = userOnlineMapByAuid.hasOwnProperty(friend.auid) ? userOnlineMapByAuid[friend.auid].online : 0
                    });

                    self.config.listeners.onFriendList(friends);
                }).catch((e)=>{
                    Logger.trace(e);
                    if(typeof self.config.listeners.onError === 'function'){
                        self.config.listeners.onError(e);
                    }
                })

                // self.config.listeners.onFriendList(response.data);
            }).catch((e)=>{
                Logger.trace(e);
                if(typeof self.config.listeners.onError === 'function'){
                    self.config.listeners.onError(e);
                }
            })
        }

        if(typeof this.config.listeners.onBlacklistForbidList === 'function'){
            const userBlackForbidListForm = new UserBlackForbidListForm();
            userBlackForbidListForm.auid = self.config.loginAuid;
            EChatIMApis.listBlackListForbid.call(userBlackForbidListForm).then((response)=>{
                self.config.listeners.onBlacklistForbidList(response.data);
            }).catch((e)=>{
                Logger.trace(e);
                if(typeof self.config.listeners.onError === 'function'){
                    self.config.listeners.onError(e);
                }
            })
        }

        if(typeof this.config.listeners.onRoomList === 'function'){
            // TODO: get user's room joined
            const roomListJoinForm = new RoomListJoinForm();
            roomListJoinForm.auid = self.config.loginAuid;
            EChatIMApis.listRoomJoin.call(roomListJoinForm).then((response)=>{
                self.config.listeners.onRoomList(response.data);
            }).catch((e)=>{
                Logger.trace(e);
                if(typeof self.config.listeners.onError === 'function'){
                    self.config.listeners.onError(e);
                }
            })
        }
    }

   /** ---------------------------- echatim sdk 高级方法, 用于外部业务拓展 -------------------------- */
    // 暴露当前的socket.io 句柄
    public getSocket(): ISocket{
        return socket;
    }
    // 暴露当前登录用户的jwt, 若用户没登录, 返回空值
    public getUserJwt(): string {
        return jwtCache.jwt;
    }
    // 暴露http常规调用接口, 使用http POST方式访问后台api
    public httpApiCall(url:string, headers:any, body: any): Promise<ApiResponse<any>> {
        const api = new HttpApi<any, any>(url, "通用http api");
        return api.rawCall(headers, body);
    }
    // 暴露socket.io常规调用接口, 使用socket.io方式访问后台api
    public socketioApiCall(url:string, body: any): Promise<ApiResponse<any>> {
        const api = new SocketIOApi<any, any>(url, "通用socket.io api");
        return api.rawCall(body);
    }
    // 设置当前登录用户的jwt到sdk
    public setUserJwt(jwt: string) {
        jwtCache.jwt = jwt;
        this.logined = true;
    }
    // 设置当前登录用户账号到sdk
    public setUserAuid(loginAuid: string) {
        this.config.loginAuid = loginAuid;
    }
    // 设置多语言设置到sdk
    public setLocale(locale: string) {
        this.config.locale = locale;
    }

    // 调用sdk im监听, 一般用于外部业务自行管理用户登录状态的场合.
    public invokeConfigImListener(){
        this.initConfigImListener();
    }
    // 暴露用户的登录接口
    public login(loginAuid:string, loginToken:string, finishHandler:()=>void): void {
        if(!this.config.key || !loginAuid || !loginToken){
            return;
        }
        const self = this;
        const form = new UserLoginForm();
        form.auid = loginAuid;
        form.token = loginToken;
        form.appKey = this.config.key;
        EChatIMApis.login.call(form).then((res:ApiResponse<string>)=>{
            Logger.info('user login ok.');
            if(res.isSucceed()){
                if(!res.data){
                    throw Error('server not return jwt yet.');
                }
                jwtCache.jwt = res.data!.toString();
                if(typeof finishHandler === 'function'){
                    finishHandler();
                }
            }
            else {
                throw Error('user login failed.');
            }
        }).catch((e:any)=>{
            Logger.trace(e);
            if(typeof self.config.listeners.onError === 'function'){
                self.config.listeners.onError(e);
            }
        });
    }
    // 获取当前SDK服务的基础地址
    public getBaseUrl():string{
        // const http = this.config.apiTransport.toUpperCase() ? this.config.apiTransport.toUpperCase() : 'http';
        const prefix = (this.config.apiTransport === 'SSL' ? 'https://' : 'http://');
        return `${prefix}${this.config.host}:${this.config.httpPort}`;
    }

}

export const echatIMSDK = new EChatIMSDK();


