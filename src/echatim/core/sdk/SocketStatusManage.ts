import Logger from "../../log/Logger";

/**
 *  socket.io 连接状态管理
 * */
export default class SocketStatusManage {
  private nativeSocket: any;
  private status: string;
  private listeners: any[];
  constructor(nativeSocket){
    this.nativeSocket = nativeSocket;
    this.status = 'DISCONNECT'; // socket 的连接状态 DISCONNECT: 断线; CONNECTING: 连接中; CONNECTED: 已连接
    this.listeners = [];
    if(typeof this.nativeSocket.__proto__.on !== 'function'){
       Logger.error(' not found \'on\' method ');
       return;
    }
    const onFunc = this.nativeSocket.on.bind(this.nativeSocket);
    // 所有客户端socket.io 支持的事件, 参考: https://blog.csdn.net/s_mantou/article/details/78087067
    onFunc('connect', (data)=>{
      this.status = 'CONNECTED';
      this.listeners.forEach(v=>v({sessionId:this.nativeSocket.id}, this.status));
    });
    onFunc('connect_error', (data)=>{
    });
    onFunc('connect_timeout', (data)=>{
    });
    onFunc('error', (data)=>{

    });
    onFunc('disconnect', (data)=>{
      this.status = 'DISCONNECT';
      this.listeners.forEach(v=>v({sessionId:this.nativeSocket.id},this.status));
    });
    onFunc('reconnect', (data)=>{

    });
    onFunc('reconnect_attempt', (data)=>{
      if(this.status !== 'CONNECTING'){
        this.status = 'CONNECTING';
        this.listeners.forEach(v=>v({sessionId:this.nativeSocket.id},this.status, data));
      }
    });
    onFunc('reconnecting', (data)=>{
      if(this.status !== 'CONNECTING'){
        this.status = 'CONNECTING';
        this.listeners.forEach(v=>v({sessionId:this.nativeSocket.id},this.status));
      }
    });
    onFunc('reconnect_error', (data)=>{

    });
    onFunc('reconnect_failed', (data)=>{

    });
    onFunc('ping', (data)=>{

    });
    onFunc('pong', (data)=> {

    });
  }
  public addListener(l){
    this.listeners.push(l);
  }
  public removeListener(l){
    this._removeArrayElement(this.listeners, l);
  }


  _removeArrayElement(arrays, val) {
    const index = arrays.indexOf(val);
    if (index > -1) {
       arrays.splice(index, 1);
    }
  };

}

