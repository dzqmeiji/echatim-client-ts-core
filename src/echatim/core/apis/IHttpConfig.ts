interface IHttpConfig {
    getBaseUrl: () => string;
    getHost: () => string;
    getPort: () => number;
    getEinfo: () => any;
    getLocale: () => string;
}
