import SocketIOApi from "./SocketIOApi";
import { Topic } from "../../common/Topic";
import { TopicProfessional } from "../../common/TopicProfessional";
import UserLoginForm from "../../model/form/UserLoginForm";
import ApiCall from "./IApi";
import MessageSendForm from "../../model/form/MessageSendForm";
import HttpApi from "./HttpApi";
import UserAddForm from "../../model/form/UserAddForm";
import UserUpdateForm from "../../model/form/UserUpdateForm";
import UserListForm from "../../model/form/UserListForm";
import UserUpdateTokenForm from "../../model/form/UserUpdateTokenForm";
import UserRefreshTokenForm from "../../model/form/UserRefreshTokenForm";
import HistoryListForm from "../../model/form/HistoryListForm";
import HistoryListSessionForm from "../../model/form/HistoryListSessionForm";
import UserFriendAddForm from "../../model/form/UserFriendAddForm";
import UserFriendDelForm from "../../model/form/UserFriendDelForm";
import UserFriendListForm from "../../model/form/UserFriendListForm";
import UserBlacklistModifyForm from "../../model/form/UserBlacklistModifyForm";
import UserBlackForbidListForm from "../../model/form/UserBlackForbidListForm";
import RoomAddForm from "../../model/form/RoomAddForm";
import RoomUpdateForm from "../../model/form/RoomUpdateForm";
import RoomListForm from "../../model/form/RoomListForm";
import RoomDelForm from "../../model/form/RoomDelForm";
import RoomAddUserForm from "../../model/form/RoomAddUserForm";
import RoomDelUserForm from "../../model/form/RoomDelUserForm";
import UserRefreshTokenDTO from "../../model/dto/UserRefreshTokenDTO";
import MessageListDTO from "../../model/dto/MessageListDTO";
import UserFriendListDTO from "../../model/dto/UserFriendListDTO";
import UserBlackForbidListDTO from "../../model/dto/UserBlackForbidListDTO";
import RoomListDTO from "../../model/dto/RoomListDTO";
import RoomDetailListDTO from "../../model/dto/RoomDetailListDTO";
import UserListDTO from "../../model/dto/UserListDTO";
import HistorySessionDTO from "../../model/dto/HistorySessionDTO";
import RoomListJoinForm from "../../model/form/RoomListJoinForm";
import UserGetDTO from "../../model/dto/UserGetDTO";
import UserGetForm from "../../model/form/UserGetForm";
import UserFriendAliasForm from "../../model/form/UserFriendAliasForm";
import UserFriendGetForm from "../../model/form/UserFriendGetForm";
import RoomGetForm from "../../model/form/RoomGetForm";
import RoomGetDTO from "../../model/dto/RoomGetDTO";
import UserFriendGetDTO from "../../model/dto/UserFriendGetDTO";
import ClientOnlineMonitorForm from "../../model/form/ClientOnlineMonitorForm";
import ClientUserOnlineForm from "../../model/form/ClientUserOnlineForm";
import UserApplyRecordsDTO from "../../model/dto/UserApplyRecordsDTO";
import UserApplyRecordsForm from "../../model/form/UserApplyRecordsForm";
import UserFriendAgreeForm from "../../model/form/UserFriendAgreeForm";
import UserFriendRejectForm from "../../model/form/UserFriendRejectForm";
import RoomSearchForm from "../../model/form/RoomSearchForm";
import RoomJoinForm from "../../model/form/RoomJoinForm";
import GitInfoDTO from "../../model/dto/GitInfoDTO";
import RoomExitForm from "../../model/form/RoomExitForm";
import HistoryRoomRecentlyForm from "../../model/form/HistoryRoomRecentlyForm";
import SystemDocumentDTO from "../../model/dto/SystemDocumentDTO";
import SystemDocumentListForm from "../../model/form/SystemDocumentListForm";
import ProtocolMessage from "../../model/protocol/ProtocolMessage";
import RoomMutedForm from "../../model/form/RoomMutedForm";
import RoomMessageCancelForm from "../../model/form/RoomMessageCancelForm";
import RoomDelAdminForm from "../../model/form/RoomDelAdminForm";
import RoomAddAdminForm from "../../model/form/RoomAddAdminForm";
import MessageCancelForm from "../../model/form/MessageCancelForm";
import RoomListAdminForm from "../../model/form/RoomListAdminForm";
import RoomListManagedForm from "../../model/form/RoomListAdminManaged";
import RoomAgreeForm from "../../model/form/RoomAgreeForm";
import RoomAdminApplyRecordsForm from "../../model/form/RoomAdminApplyRecordsForm";
import RoomApplyRecordsForm from "../../model/form/RoomApplyRecordsForm";
import RoomRejectForm from "../../model/form/RoomRejectForm";
import UserMyApplyRecordsForm from "../../model/form/UserMyApplyRecordsForm";


export default class EChatIMApis {
    public static login: ApiCall<UserLoginForm, string> = new SocketIOApi<UserLoginForm, string>(
        Topic.CONNECTION.topic_name + "/" + Topic.CONNECTION.METHOD.AUTHORITY_REQUEST, "用户登录/请求授权", false);
    public static heartbeat: ApiCall<ProtocolMessage, string> = new SocketIOApi<ProtocolMessage, string>(
      Topic.CONNECTION.topic_name + "/" + Topic.CONNECTION.METHOD.HEARTBEAT, "应用层心跳包", false);
    public static addOnlineMonitor:ApiCall<ClientOnlineMonitorForm, void> = new SocketIOApi<ClientOnlineMonitorForm, void>(
        Topic.APP_CLIENT.topic_name + "/" + Topic.APP_CLIENT.METHOD.ADD_ONLINE_MONITOR,
        "设置用户的在线监听", true);
    public static removeOnlineMonitor:ApiCall<ClientOnlineMonitorForm, void> = new SocketIOApi<ClientOnlineMonitorForm, void>(
      Topic.APP_CLIENT.topic_name + "/" + Topic.APP_CLIENT.METHOD.REMOVE_ONLINE_MONITOR,
      "移除用户的在线监听", true);

    // ========== http interface ==========

    public static  sendMessage: ApiCall<MessageSendForm, void> = new HttpApi<MessageSendForm, void>(
        Topic.APP_UPSTREAM_MESSAGE.base_uri + "/" + Topic.APP_UPSTREAM_MESSAGE.METHOD.SEND,
        "APP跟IM用户发送消息", "POST",true);
    public static  cancelMessage: ApiCall<MessageCancelForm, void> = new HttpApi<MessageCancelForm, void>(
      Topic.APP_UPSTREAM_MESSAGE.base_uri + "/" + Topic.APP_UPSTREAM_MESSAGE.METHOD.CANCEL_MESSAGE,
      "撤回消息", "POST",true);
    // message
    // public static sendMessageHttp:ApiCall<MessageSendForm, void> = new HttpApi<MessageSendForm, void>(
    //     Topic.APP_UPSTREAM_MESSAGE.base_uri + "/" + Topic.APP_UPSTREAM_MESSAGE.METHOD.SEND,
    //     "APP跟IM用户发送消息", "POST", true);
    // user
    public static userAdd:ApiCall<UserAddForm, void> = new HttpApi<UserAddForm, void>(
        Topic.APP_USER.base_uri + "/" + Topic.APP_USER.METHOD.ADD,
        "添加用户", "POST", true);
    public static userUpdate:ApiCall<UserUpdateForm, void> = new HttpApi<UserUpdateForm, void>(
        Topic.APP_USER.base_uri + "/" + Topic.APP_USER.METHOD.UPDATE,
        "更新用户", "POST", true);
    public static userList:ApiCall<UserListForm, Array<UserListDTO>> = new HttpApi<UserListForm, Array<UserListDTO>>(
        Topic.APP_USER.base_uri + "/" + Topic.APP_USER.METHOD.LIST,
        "用户列表", "POST", true);
    public static userGet:ApiCall<UserGetForm, UserGetDTO> = new HttpApi<UserGetForm, UserGetDTO>(
        Topic.APP_USER.base_uri + "/" + Topic.APP_USER.METHOD.GET,
        "获取用户信息", "POST", true);
    public static userUpdateToken:ApiCall<UserUpdateTokenForm, void> = new HttpApi<UserUpdateTokenForm, void>(
        Topic.APP_USER.base_uri + "/" + Topic.APP_USER.METHOD.UPDATE_TOKEN,
        "更新用户Token", "POST", true);
    public static userRefreshToken:ApiCall<UserRefreshTokenForm, UserRefreshTokenDTO> = new HttpApi<UserRefreshTokenForm, UserRefreshTokenDTO>(
        Topic.APP_USER.base_uri + "/" + Topic.APP_USER.METHOD.REFRESH_TOKEN,
        "刷新用户Token", "POST", true);
    // history message
    public static historyListMessage:ApiCall<HistoryListForm, Array<MessageListDTO>> = new HttpApi<HistoryListForm, Array<MessageListDTO>>(
        Topic.APP_HISTORY_MESSAGE.base_uri + "/" + Topic.APP_HISTORY_MESSAGE.METHOD.LIST_MESSAGE,
        "获取用户历史信息", "POST", true);
    public static historyListSession:ApiCall<HistoryListSessionForm, Array<HistorySessionDTO>> = new HttpApi<HistoryListSessionForm, Array<HistorySessionDTO>>(
        Topic.APP_HISTORY_MESSAGE.base_uri + "/" + Topic.APP_HISTORY_MESSAGE.METHOD.LIST_SESSION,
        "获取用户历史会话", "POST", true);
    public static listRoomRecently:ApiCall<HistoryRoomRecentlyForm, Array<MessageListDTO>> = new HttpApi<HistoryRoomRecentlyForm, Array<MessageListDTO>>(
      Topic.APP_HISTORY_MESSAGE.base_uri + "/" + Topic.APP_HISTORY_MESSAGE.METHOD.LIST_ROOM_RECENTLY,
      "列出用户加入的群最新一条记录", "POST", true);

    // user relation
    public static addFriend:ApiCall<UserFriendAddForm, void> = new HttpApi<UserFriendAddForm, void>(
        Topic.APP_USER_RELATION.base_uri + "/" + Topic.APP_USER_RELATION.METHOD.ADD_FRIEND,
        "添加朋友", "POST", true);
    public static delFriend:ApiCall<UserFriendDelForm, void> = new HttpApi<UserFriendDelForm, void>(
        Topic.APP_USER_RELATION.base_uri + "/" + Topic.APP_USER_RELATION.METHOD.DEL_FRIEND,
        "删除朋友", "POST", true);
    public static listFriends:ApiCall<UserFriendListForm, UserFriendListDTO> = new HttpApi<UserFriendListForm, UserFriendListDTO>(
        Topic.APP_USER_RELATION.base_uri + "/" + Topic.APP_USER_RELATION.METHOD.LIST_FRIENDS,
        "获取朋友列表", "POST", true);
    public static getFriend:ApiCall<UserFriendGetForm, UserFriendGetDTO> = new HttpApi<UserFriendGetForm, UserFriendGetDTO>(
        Topic.APP_USER_RELATION.base_uri + "/" + Topic.APP_USER_RELATION.METHOD.GET_FRIEND,
        "获取好友", "POST", true);
    public static modifyBlacklistForbid:ApiCall<UserBlacklistModifyForm, void> = new HttpApi<UserBlacklistModifyForm, void>(
        Topic.APP_USER_RELATION.base_uri + "/" + Topic.APP_USER_RELATION.METHOD.MODIFY_BLACKLIST_FORBID,
        "修改朋友黑名单/禁言", "POST", true);
    public static listBlackListForbid:ApiCall<UserBlackForbidListForm, UserBlackForbidListDTO> = new HttpApi<UserBlackForbidListForm, UserBlackForbidListDTO>(
        Topic.APP_USER_RELATION.base_uri + "/" + Topic.APP_USER_RELATION.METHOD.LIST_BLACKLIST_FORBID,
        "获取朋友黑名单/禁言列表", "POST", true);
    public static modifyAlias:ApiCall<UserFriendAliasForm, void> = new HttpApi<UserFriendAliasForm, void>(
        Topic.APP_USER_RELATION.base_uri + "/" + Topic.APP_USER_RELATION.METHOD.MODIFY_ALIAS,
        "修改朋友别名", "POST", true);
    public static applyRecord:ApiCall<UserApplyRecordsForm, void> = new HttpApi<UserApplyRecordsForm, UserApplyRecordsDTO>(
      Topic.APP_USER_RELATION.base_uri + "/" + Topic.APP_USER_RELATION.METHOD.APPLY_RECORD,
      "显示陌生人好友申请记录", "POST", true);
    public static myApplyRecord:ApiCall<UserMyApplyRecordsForm, void> = new HttpApi<UserMyApplyRecordsForm, UserApplyRecordsDTO>(
      Topic.APP_USER_RELATION.base_uri + "/" + Topic.APP_USER_RELATION.METHOD.MY_APPLY_RECORD,
      "显示我的好友申请记录", "POST", true);
    public static agreeFriend:ApiCall<UserFriendAgreeForm, void> = new HttpApi<UserFriendAgreeForm, void>(
      Topic.APP_USER_RELATION.base_uri + "/" + Topic.APP_USER_RELATION.METHOD.AGREE_FRIEND,
      "同意陌生人为好友", "POST", true);
    public static rejectFriend:ApiCall<UserFriendRejectForm, void> = new HttpApi<UserFriendRejectForm, void>(
      Topic.APP_USER_RELATION.base_uri + "/" + Topic.APP_USER_RELATION.METHOD.REJECT_FRIEND,
      "拒绝陌生人为好友", "POST", true);

    // room
    public static addRoom:ApiCall<RoomAddForm, number> = new HttpApi<RoomAddForm, number>(
        Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.ADD,
        "添加群", "POST", true);
    public static updateRoom:ApiCall<RoomUpdateForm, void> = new HttpApi<RoomUpdateForm, void>(
        Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.UPDATE,
        "更新群", "POST", true);
    public static listRoom:ApiCall<RoomListForm, Array<RoomListDTO>> = new HttpApi<RoomListForm, Array<RoomListDTO>>(
        Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.LIST,
        "获取群列表", "POST", true);
    public static listRoomJoin:ApiCall<RoomListJoinForm, Array<RoomListDTO>> = new HttpApi<RoomListJoinForm, Array<RoomListDTO>>(
        Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.LIST_JOIN,
        "获取用户加入的群", "POST", true);
    public static delRoom:ApiCall<RoomDelForm, void> = new HttpApi<RoomDelForm, void>(
        Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.DEL,
        "删除群", "POST", true);
    public static getRoom:ApiCall<RoomGetForm, RoomGetDTO> = new HttpApi<RoomGetForm, RoomGetDTO>(
        Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.GET,
        "获取单个群信息", "POST", true);
    public static searchRoom:ApiCall<RoomSearchForm, void> = new HttpApi<RoomSearchForm, Array<RoomListDTO>>(
      Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.SEARCH,
      "搜索群(通过name)", "POST", true);
    public static joinRoom:ApiCall<RoomJoinForm, void> = new HttpApi<RoomJoinForm, void>(
      Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.JOIN,
      "加入群", "POST", true);
    public static exitRoom:ApiCall<RoomExitForm, void> = new HttpApi<RoomExitForm, void>(
      Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.EXIT,
      "退出群", "POST", true);
    public static mutedRoom:ApiCall<RoomMutedForm, void> = new HttpApi<RoomMutedForm, void>(
      Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.MUTED,
      "全员禁言", "POST", true);
    public static addRoomAdmin:ApiCall<RoomAddAdminForm, void> = new HttpApi<RoomAddAdminForm, void>(
      Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.ADD_ROOM_ADMIN,
      "添加群管理员", "POST", true);
    public static removeRoomAdmin:ApiCall<RoomDelAdminForm, void> = new HttpApi<RoomDelAdminForm, void>(
      Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.REMOVE_ROOM_ADMIN,
      "移除群管理员", "POST", true);
    public static listRoomAdmin:ApiCall<RoomListAdminForm, void> = new HttpApi<RoomListAdminForm, void>(
      Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.LIST_ROOM_ADMIN,
      "群管理员列表", "POST", true);
    public static listRoomManaged:ApiCall<RoomListManagedForm, void> = new HttpApi<RoomListManagedForm, void>(
      Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.LIST_ROOM_MANAGED,
      "获取当前用户管理的群列表", "POST", true);
    public static cancelRoomMessage:ApiCall<RoomMessageCancelForm, void> = new HttpApi<RoomMessageCancelForm, void>(
      Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.CANCEL_ROOM_MESSAGE,
      "移除群成员的消息", "POST", true);

    public static listRoomMember:ApiCall<RoomListForm, Array<RoomDetailListDTO>> = new HttpApi<RoomListForm, Array<RoomDetailListDTO>>(
        Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.LIST_MEMBER,
        "列出群成员", "POST", true);
    public static addRoomMember:ApiCall<RoomAddUserForm, void> = new HttpApi<RoomAddUserForm, void>(
        Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.ADD_USER,
        "添加群成员", "POST", true);
    public static delRoomMember:ApiCall<RoomDelUserForm, void> = new HttpApi<RoomDelUserForm, void>(
        Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.DEL_USER,
        "删除群成员", "POST", true);


    public static roomApplyRecord:ApiCall<RoomApplyRecordsForm, void> = new HttpApi<RoomApplyRecordsForm, void>(
      Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.APPLY_RECORD,
      "显示自己的入群申请记录", "POST", true);
    public static roomAdminApplyRecord:ApiCall<RoomAdminApplyRecordsForm, void> = new HttpApi<RoomAdminApplyRecordsForm, void>(
      Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.ADMIN_APPLY_RECORD,
      "群管理员显示用户入群申请记录", "POST", true);
    public static agreeRoom:ApiCall<RoomAgreeForm, void> = new HttpApi<RoomAgreeForm, void>(
      Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.AGREE_ROOM,
      "同意用户加入群", "POST", true);
    public static rejectRoom:ApiCall<RoomRejectForm, void> = new HttpApi<RoomRejectForm, void>(
      Topic.APP_ROOM.base_uri + "/" + Topic.APP_ROOM.METHOD.REJECT_ROOM,
      "拒绝用户加入群", "POST", true);

    public static userOnline:ApiCall<ClientUserOnlineForm, void> = new HttpApi<ClientUserOnlineForm, void>(
        Topic.APP_CLIENT.base_uri + "/" + Topic.APP_CLIENT.METHOD.USER_ONLINE,
        "列出用户的在线状态", "POST", true);

    // ----- 专业版API ------
    public static authRegister:ApiCall<any, void> = new HttpApi<any, void>(
      TopicProfessional.APP_USER_AUTH.base_uri + "/" + TopicProfessional.APP_USER_AUTH.METHOD.REGISTER,
      "用户/游客注册", "POST", false); // 这里仅允许游客注册, 若允许用户注册会带来风险, 参数需要带appKey
    public static authLogin:ApiCall<any, void> = new HttpApi<any, void>(
      TopicProfessional.APP_USER_AUTH.base_uri + "/" + TopicProfessional.APP_USER_AUTH.METHOD.LOGIN,
      "用户/游客登入", "POST", false);
    public static authLogout:ApiCall<any, void> = new HttpApi<any, void>(
      TopicProfessional.APP_USER_AUTH.base_uri + "/" + TopicProfessional.APP_USER_AUTH.METHOD.LOGOUT,
      "用户/游客登出", "POST", true);
    public static authInfo:ApiCall<any, void> = new HttpApi<any, void>(
      TopicProfessional.APP_USER_AUTH.base_uri + "/" + TopicProfessional.APP_USER_AUTH.METHOD.INFO,
      "获取用户/游客的信息", "POST", false);

    // system
    public static systemInfo:ApiCall<any, GitInfoDTO> = new HttpApi<any, GitInfoDTO>(
      Topic.APP_SYSTEM.base_uri + "/" + Topic.APP_SYSTEM.METHOD.INFO,
      "后台系统信息", "POST", false);

    // system-document
    public static listSystemDocument:ApiCall<SystemDocumentListForm, SystemDocumentDTO> = new HttpApi<SystemDocumentListForm, SystemDocumentDTO>(
      Topic.APP_SYSTEM_DOCUMENT.base_uri + "/" + Topic.APP_SYSTEM_DOCUMENT.METHOD.LIST_DOCUMENT,
      "后台系统文档", "POST", false);
}


