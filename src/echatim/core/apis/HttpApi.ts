import ProtocolMessage from "../../model/protocol/ProtocolMessage";
import IApi from "./IApi";
import { echatIMSDK } from "../sdk/EChatIMSDK";
import ApiResponse from "./ApiResponse";
import Beans from "../../common/utils";
import Encrypt from "../../common/Encrypt";
import R from "../../model/protocol/R";
import { jwtCache } from "../cache/jwt";
import UserLoginForm from "../../model/form/UserLoginForm";
import EChatIMApis from "./EChatIMApis";
import Logger from "../../log/Logger";
import Fetch from "../fileclient/support/Fetch";


export default class HttpApi<K extends ProtocolMessage, V> implements IApi<K,V> {
    url: string;
    name: string | undefined;
    needAuth: boolean | undefined;
    method: string | undefined;


    constructor(url = "", name: string, method?:string, needAuth?: boolean) {
        this.url = url;
        this.name = name;
        this.needAuth = needAuth;
        this.method = method;
    }

    call(params?: K): Promise<ApiResponse<V>> {
        if(params === undefined){
            params = new ProtocolMessage() as K;
        }
        let url = this.getBaseUrl() +  (this.url.startsWith("/") ? this.url : "/" + this.url);
        let requestMethod = this.method === undefined ? 'POST' : this.method;

        // 使用数字签名
        if(this.needAuth && !jwtCache.jwt){
            let self = this;
            return new Promise<ApiResponse<V>>(function (resolve, reject) {
                const form = new UserLoginForm();
                form.auid = echatIMSDK.getState().loginAuid;
                form.token = echatIMSDK.getState().loginToken;
                form.appKey = echatIMSDK.getState().key;
                const tokenResponse: Promise<ApiResponse<string>> = EChatIMApis.login.call(form);
                tokenResponse.then((res) => {
                    // resolve(res);
                    if(res.isSucceed()){
                        if(!res.data){
                            throw Error('server not return jwt yet.');
                        }
                        jwtCache.jwt = res.data!.toString();
                        // return res.data.data;
                        // re-call again
                        const apiResponse: Promise<ApiResponse<K>> = self.call(params);
                        apiResponse.then((res) => {
                            resolve(res);
                        }).catch(e => {
                            reject(e);
                        })
                    }
                    else {
                        throw Error('get jwt failed.');
                    }

                }).catch((e) =>{
                    Logger.info(`auto login failed, url=${self.url}`);
                    reject(e);
                })
            });
        }
        Logger.info(`==> [${requestMethod}] ${url} call with params:` + Beans.json(params));
        const headers = {
            'Content-Type':'application/json',
            'Request-Id':new Date().getTime() + '' + parseInt(Math.random() * 1000 + ''),
            'timestamp':new Date().getTime(),
        };
        if(this.needAuth){
            headers['authorization'] = 'client ' + jwtCache.jwt;
        }
        if(this.getEinfo().enable){
            headers['Api-Encrypt'] = 'true';
        }
        if(this.getLocale()){
            headers['accept-language'] = this.getLocale();
        }
        // const request: any = {method: requestMethod, body: params};
        const request: any = {method: requestMethod, mode: 'cors', headers:headers, body: this.encryptForm(Beans.json(params))};
        return this.httpFetch(url, request);

    }



    rawCall(headers:any, params?: K): Promise<ApiResponse<V>> {
        if(params === undefined){
            params = new ProtocolMessage() as K;
        }

        let url = this.url;
        let requestMethod = 'POST';
        Logger.info(`==> [${requestMethod}] ${url} call with params:` + Beans.json(params));
        if(this.getEinfo().enable){
            headers['Api-Encrypt'] = 'true';
        }
        if(this.getLocale()){
            headers['accept-language'] = this.getLocale();
        }
        // const request: any = {method: requestMethod, body: params};
        const request: any = {method: requestMethod, mode: 'cors', headers:headers, body: this.encryptForm(Beans.json(params))};
        return this.httpFetch(url, request);
    }


    public response2ApiResponse( response: R<V>|any): ApiResponse<V> {
        const apiResponse: ApiResponse<V> = new ApiResponse<V>(true);
        if(this.getEinfo().enable && response.hasOwnProperty('t') && response.hasOwnProperty('c') && response.t && response.c){
            const content = Encrypt.decrypt(response.c, this.getEinfo().k);
            const r:R<any> = Beans.bean(content) as R<any>;
            apiResponse.data = r.data;
            apiResponse.errorCode = r.code;
            apiResponse.errorMessage = r.msg;
        }
        else {
            apiResponse.data = response.data;
            apiResponse.errorCode = response.code;
            apiResponse.errorMessage = response.msg;
        }
        apiResponse.raw = response ? Beans.json(response) : '';

        return apiResponse;
    }

    getBaseUrl: () => string = function(){
        const prefix = (echatIMSDK.getState().apiTransport === 'SSL' ? 'https://' : 'http://');
        return `${prefix}${echatIMSDK.getState().host}:${echatIMSDK.getState().httpPort}`;
    }
    getHost: () => string = function(){
        return echatIMSDK.getState().host;
    }
    getPort: () => number = function(){
        return echatIMSDK.getState().httpPort;
    }
    getEinfo: () => any = function(){
        return echatIMSDK.getState().einfo;
    }
    getLocale: () => string = function(){
        return echatIMSDK.getState().locale;
    }

    private encryptForm(form) {
        if(this.getEinfo().enable){
            return Beans.json({
                t:this.getEinfo().t,
                c:Encrypt.encrypt(form, this.getEinfo().k)
            })
        }
        return form;
    }


    private httpFetch(url:string, request:any):Promise<ApiResponse<V>>{
        /*IFTRUE_WXAPP*/
        // @ts-ignore
        if(wx === undefined){
            throw new Error('wx handle not exist');
        }
        return new Promise<ApiResponse<V>>(function (resolve, reject) {
            // @ts-ignore
            wx.request({
                method: request.method,
                url: url, //仅为示例，并非真实的接口地址
                data: Beans.bean(request.body),
                header: request.headers,
                success (res) {
                    resolve(res.data);
                },
                fail(res){
                    reject(res.data);
                }
            });
        });
        /*FITRUE_WXAPP*/

        /*IFTRUE_WEBAPP*/
        let webfetch = Fetch.getFetchToolkit();
        return webfetch(url as string, request).then(response =>{
            return response.json();
        }).then(res =>{
            Logger.info(`==> [${request.method}] ${url} back:` + Beans.json(res));
            const resp = this.response2ApiResponse(res);
            if(resp.isFailed()){
                return Promise.reject(resp);
            }
            return Promise.resolve(this.response2ApiResponse(res));
        });
        /*FITRUE_WEBAPP*/

        /*IFTRUE_RNAPP*/
        let rnfetch = Fetch.getFetchToolkit();
        return rnfetch(url as string, request).then(response =>{
            return response.json();
        }).then(res =>{
            Logger.info(`==> [${request.method}] ${url} back:` + Beans.json(res));
            const resp = this.response2ApiResponse(res);
            if(resp.isFailed()){
                return Promise.reject(resp);
            }
            return Promise.resolve(this.response2ApiResponse(res));
        });
        /*FITRUE_RNAPP*/

        /*IFTRUE_UNIAPP*/
        let rnfetch = Fetch.getFetchToolkit();
        return rnfetch(url as string, request).then(response =>{
            return response.json();
        }).then(res =>{
            Logger.info(`==> [${request.method}] ${url} back:` + Beans.json(res));
            const resp = this.response2ApiResponse(res);
            if(resp.isFailed()){
                return Promise.reject(resp);
            }
            return Promise.resolve(this.response2ApiResponse(res));
        });
        /*FITRUE_UNIAPP*/
    }
}
