/*
 * test case for echatim sdk.
 */
// const math = require('../src/math');

import { echatIMSDK, EChatIMSDK, EChatIMSDKConfig } from "../core/sdk/EChatIMSDK";
import Logger from "../log/Logger";
import Beans from "../common/utils";
import UserAddForm from "../model/form/UserAddForm";
import EChatIMApis from "../core/apis/EChatIMApis";
import { Topic } from "../common/Topic";
import UserUpdateForm from "../model/form/UserUpdateForm";
import UserListForm from "../model/form/UserListForm";
import UserUpdateTokenForm from "../model/form/UserUpdateTokenForm";
import MessageSendForm from "../model/form/MessageSendForm";
import R from "../model/protocol/R";
import MessageReceiveDTO from "../model/dto/MessageReceiveDTO";
import HistorySessionDTO from "../model/dto/HistorySessionDTO";
import UserFriendListDTO from "../model/dto/UserFriendListDTO";
import UserBlackForbidListDTO from "../model/dto/UserBlackForbidListDTO";
import RoomListDTO from "../model/dto/RoomListDTO";

test('_0_test_topic_ok', () => {
    console.log("topic");

});

async function sdkInitBeforeTest(sdkConfig){
    function sdkInit(): Promise<EChatIMSDK> {
        return new Promise<EChatIMSDK>(function (resolve, reject) {
            reject = reject;

            const config:EChatIMSDKConfig = new EChatIMSDKConfig();
            config.host = sdkConfig.host;
            config.httpPort = sdkConfig.httpPort;
            config.socketPort = sdkConfig.socketPort;
            config.key = 'TSDKTEST00001';
            config.secret = '';
            config.apiTransport = '';
            config.loginAuid = sdkConfig.loginAuid;
            config.loginToken = sdkConfig.loginToken;
            config.listeners = sdkConfig.listeners;
            echatIMSDK.init(config, function (sdk:EChatIMSDK) {
                if(sdk){
                    Logger.info('echatIMSDK 成功连接, 可以使用 EChatIMApis 请求数据了.');
                    resolve(sdk);
                }
                else {
                    throw Error("echatIMSDK 初始化失败");
                }

            })
        });
    }
    const sdk =  await sdkInit()
        .then((sdk: EChatIMSDK) => {
            // Logger.info(`response:${JSON.stringify(msg)}`);
            return sdk;
        })
        .catch((e) => {
            Logger.trace(e);
        });

    (sdk as EChatIMSDK).getSocket().listen(Topic.APP_DOWNSTREAM_MESSAGE.topic_name, function(ack){
        console.log(Topic.APP_DOWNSTREAM_MESSAGE.topic_name + ' receive message' + Beans.json(ack));
        // Logger.info('topic.message ack:' + Beans.json(ack));
    });
    return sdk;
}


test('_0_test_user_apis', async () => {
    const  host = "localhost";
    const  socketPort = 9092;
    const  httpPort = 8082;
    const  userAuid = "admin";
    const  userToken = "admin";
    await sdkInitBeforeTest({
        host:host,
        httpPort:httpPort,
        socketPort:socketPort,
        loginAuid:userAuid,
        loginToken:userToken,
        listeners:{
            onConnected: ()=>{
                console.log("connected.");
            },
            onDisConnected: ()=>{
                console.log("disConnected.");
            },
            onMessageReceive:(message: R<MessageReceiveDTO>)=>{
                console.log(message);
            },
            onSessionList:(list:R<Array<HistorySessionDTO>>)=>{
                console.log(list);
            },
            onFriendList:(list:R<Array<UserFriendListDTO>>)=>{
                console.log(list);
            },
            onBlacklistForbidList:(list:R<Array<UserBlackForbidListDTO>>)=>{
                console.log(list);
            },
            onRoomList:(list:R<Array<RoomListDTO>>)=>{
                console.log(list);
            }
        }
    });

    const TEST_USER_AUID = "admin_" + new Date().getTime();
    // 添加用户
    const form = new UserAddForm();
    form.auid = TEST_USER_AUID;
    form.name = form.auid;
    form.token = form.auid;
    form.gender = 1;
    const result = await EChatIMApis.userAdd.call(form)
        .then((res) => {
            return res;
        }).catch((e)=>{
            Logger.trace(e);
        });
    Logger.info(`responseMsg:${JSON.stringify(result)}`);
    // 更新用户
    const userUpdateForm = new UserUpdateForm();
    userUpdateForm.auid = TEST_USER_AUID;
    userUpdateForm.name = userUpdateForm.auid;
    const responseUserUpdate = await EChatIMApis.userUpdate.call(form)
        .then((res) => {
            return res;
        }).catch((e)=>{
            Logger.trace(e);
        });
    Logger.info(`responseMsg:${JSON.stringify(responseUserUpdate)}`);
    // 用户列表
    const userListForm = new UserListForm();
    userListForm.auids = [TEST_USER_AUID];
    const responseUserList = await EChatIMApis.userList.call(userListForm)
        .then((res) => {
            return res;
        }).catch((e)=>{
            Logger.trace(e);
        });
    Logger.info(`responseMsg:${JSON.stringify(responseUserList)}`);

    // 用户更新Token
    const userUpdateTokenForm = new UserUpdateTokenForm();
    userUpdateTokenForm.auid = TEST_USER_AUID;
    userUpdateTokenForm.token = "123456";
    const responseUserUpdateToken = await EChatIMApis.userUpdateToken.call(userUpdateTokenForm)
        .then((res) => {
            return res;
        }).catch((e)=>{
            Logger.trace(e);
        });
    Logger.info(`responseMsg:${JSON.stringify(responseUserUpdateToken)}`);

});



test('_1_test_sdk_listeners', async () => {
    const  host = "localhost";
    const  socketPort = 9092;
    const  httpPort = 8082;
    const  userAuid = "admin";
    const  userToken = "admin";
    await sdkInitBeforeTest({
        host:host,
        httpPort:httpPort,
        socketPort:socketPort,
        loginAuid:userAuid,
        loginToken:userToken,
        listeners:{
            onConnected: ()=>{
                console.log("connected.");
            },
            onDisConnected: ()=>{
                console.log("disConnected.");
            },
            onMessageReceive:(message: R<MessageReceiveDTO>)=>{
                console.log(message);
            },
            onSessionList:(list:R<Array<HistorySessionDTO>>)=>{
                console.log(list);
            },
            onFriendList:(list:R<Array<UserFriendListDTO>>)=>{
                console.log(list);
            },
            onBlacklistForbidList:(list:R<Array<UserBlackForbidListDTO>>)=>{
                console.log(list);
            },
            onRoomList:(list:R<Array<RoomListDTO>>)=>{
                console.log(list);
            }
        }
    });

});



test('_2_test_send_message', async () => {
    const  host = "localhost";
    const  socketPort = 9092;
    const  httpPort = 8082;
    const  userAuid = "admin";
    const  userToken = "admin";
    await sdkInitBeforeTest({
        host:host,
        httpPort:httpPort,
        socketPort:socketPort,
        loginAuid:userAuid,
        loginToken:userToken,
    });

    // const TEST_USER_AUID = "admin_" + new Date().getTime();
    const content = "jest_msg_" + new Date().getTime();
    const toUser = "admin";
    const form = new MessageSendForm();
    form.body = content;
    form.fromUser = "admin";
    form.toTarget = toUser;
    form.way = "P2P";
    form.type = "TEXT";
    const response = await EChatIMApis.sendMessage.call(form)
        .then((res) => {
            return res;
        }).catch((e)=>{
            Logger.trace(e);
        });
    Logger.info(`responseMsg:${JSON.stringify(response)}`);
});

