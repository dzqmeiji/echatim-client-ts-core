import ProtocolMessage from "../protocol/ProtocolMessage";

export default class RoomAddAdminForm extends ProtocolMessage {
    public rid = 0; // 群ID
    public owner = ""; // 群主auid
    public members = []; // 要添加的群管理员
}
