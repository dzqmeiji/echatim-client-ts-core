import ProtocolMessage from "../protocol/ProtocolMessage";

export default class MessageCancelForm extends ProtocolMessage {
    public  msgId = ""; // 消息id
    public  auid = ""; // 当前用户auid
}
