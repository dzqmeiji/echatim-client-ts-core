import ProtocolMessage from "../protocol/ProtocolMessage";

export default class RoomAddForm extends ProtocolMessage {
    public name = ""; // 群名
    public owner = ""; // 群主auid
    public members = new Array<string>(); // 群成员, 无需再加owner自己的账号
    public announce = ""; // 群公告

    public introduce = ""; // 群简介
    public inviteText = ""; // 邀请文字
    public avatar = ""; // 群LOGO,头像
    public confJoinmode = ""; // 群加入模式
    public confBeinvite = ""; // 群员被邀请方式
    public confInviteother = ""; // 群员邀请权限(OWNER:仅群主;ALL:任何人也可以)
    public confUpdate = ""; // 群信息更新权限(OWNER:仅群主;ALL:任何人也可以)
    public maxMember = 100; // 群最大成员数量
}
