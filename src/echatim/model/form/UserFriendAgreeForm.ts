import ProtocolMessage from "../protocol/ProtocolMessage";

export default class UserFriendAgreeForm extends ProtocolMessage {
    public applyRecordId = 0; // 申请记录id
    public auid = ""; // 当前用户uid
    public targetAuid = ""; // 目标用户auid
}

