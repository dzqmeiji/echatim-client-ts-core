import ProtocolMessage from "../protocol/ProtocolMessage";

export default class HistoryListSessionForm extends ProtocolMessage {
    public auid = ""; // 用户auid
    public startTimestamp = 0; // 会话开始时间撮(毫秒)
    public endTimestamp = 0; // 会话结束时间撮(毫秒)
    public startTime = new Date();
    public endTime = new Date();
}
