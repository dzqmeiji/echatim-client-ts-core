import ProtocolMessage from "../protocol/ProtocolMessage";

export default class RoomMutedForm extends ProtocolMessage {
    public rid = 0; // 群ID
    public owner = ""; // 群主auid
    public muted = 0; // 设置全员禁言(1:是 0:否)
}
