import ProtocolMessage from "../protocol/ProtocolMessage";

export default class HistoryListForm extends ProtocolMessage {
    public fromUser = ""; // 用户auid
    public toTarget = ""; // 用户auid/ 群uid
    public way = ""; // 发送方式 P2P/P2R
    public startTimestamp = 0; // 开始时间撮(毫秒)
    public endTimestamp = 0; // 结束时间撮(毫秒)
    public limit = 10; // 消息条数最大值(默认值10条)

}