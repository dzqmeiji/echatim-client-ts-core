import ProtocolMessage from "../protocol/ProtocolMessage";

export default class UserUpdateTokenForm extends ProtocolMessage {
    public auid = ""; // 要添加的用户uid
    public token = ""; // 用户登录token

}
