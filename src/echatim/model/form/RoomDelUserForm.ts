import ProtocolMessage from "../protocol/ProtocolMessage";

export default class RoomDelUserForm extends ProtocolMessage {
    public rid = 0; // 群ID
    public owner = ""; // 群主auid
    public members = new Array<string>(); // 要移除的群成员
    public ex = ""; // 扩展字段
}
