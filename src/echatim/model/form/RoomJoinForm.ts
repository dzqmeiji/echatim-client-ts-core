import ProtocolMessage from "../protocol/ProtocolMessage";

export default class RoomJoinForm extends ProtocolMessage {
    public rid = 0; // 群ID
    public auid = ""; // 加入的用户auid
}
