import ProtocolMessage from "../protocol/ProtocolMessage";

export default class UserFriendListForm extends ProtocolMessage {
    public auid = ""; // 要添加的用户uid
    public updatetime = 0; // 返回该时间戳之后更新的好友列表(不填则显示全部)
}

