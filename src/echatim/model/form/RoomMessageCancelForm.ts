import ProtocolMessage from "../protocol/ProtocolMessage";

export default class RoomMessageCancelForm extends ProtocolMessage {
    public rid = 0; // 群ID
    public owner = ""; // 群主auid
    public msgId = ""; // 消息id
    public senderAuid = ""; // 消息发送者auid
}
