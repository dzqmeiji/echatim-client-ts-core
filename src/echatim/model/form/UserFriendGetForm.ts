import ProtocolMessage from "../protocol/ProtocolMessage";

export default class UserFriendGetForm extends ProtocolMessage {
    public auid = ""; // 用户uid
    public targetAuid = ""; // 要获取的朋友uid
}

