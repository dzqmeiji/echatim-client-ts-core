import ProtocolMessage from "../protocol/ProtocolMessage";

export default class RoomListAdminForm extends ProtocolMessage {
    public rid = 0; // 群ID
    public owner = ""; // 群主auid
}
