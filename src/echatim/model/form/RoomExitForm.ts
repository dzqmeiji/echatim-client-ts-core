import ProtocolMessage from "../protocol/ProtocolMessage";

export default class RoomExitForm extends ProtocolMessage {
    public rid = 0; // 群ID
    public auid = ""; // 退出的用户auid
}
