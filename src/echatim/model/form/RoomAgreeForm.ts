import ProtocolMessage from "../protocol/ProtocolMessage";

export default class RoomAgreeForm extends ProtocolMessage {
    public applyRecordId = ""; // 当前用户uid
    public auid = ""; // 当前用户uid
    public targetRid = ""; // 当前用户uid
    public targetAuid = ""; // 当前用户uid
}
