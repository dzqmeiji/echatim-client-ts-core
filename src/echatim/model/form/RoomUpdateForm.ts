import ProtocolMessage from "../protocol/ProtocolMessage";

export default class RoomUpdateForm extends ProtocolMessage {
    public rid = 0; // 群ID
    public name = ""; // 群名
    public owner = ""; // 群主auid
    public announce = ""; // 群公告

    public introduce = ""; // 群简介
    public avatar = ""; // 群LOGO,头像
    public confJoinmode = ""; // 群加入模式
    public confBeinvite = ""; // 群员被邀请方式
    public confInviteother = ""; // 群员邀请权限(OWNER:仅群主;ALL:任何人也可以)
    public confUpdate = ""; // 群信息更新权限(OWNER:仅群主;ALL:任何人也可以)
    public maxMember = 100; // 群最大成员数量
}
