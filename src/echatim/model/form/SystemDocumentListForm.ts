import ProtocolMessage from "../protocol/ProtocolMessage";

export default class SystemDocumentListForm extends ProtocolMessage {
    public categories = []; // 文档分类列表
}