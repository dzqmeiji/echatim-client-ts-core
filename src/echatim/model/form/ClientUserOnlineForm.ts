import ProtocolMessage from "../protocol/ProtocolMessage";

export default class ClientUserOnlineForm extends ProtocolMessage {
    public auids = new Array<string>(); // 要查询的用户auids
}
