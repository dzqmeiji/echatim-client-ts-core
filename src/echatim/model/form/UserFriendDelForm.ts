import ProtocolMessage from "../protocol/ProtocolMessage";

export default class UserFriendDelForm extends ProtocolMessage {
    public auid = ""; // 要添加的用户uid
    public targetAuid = ""; // 要删除的朋友uid
}

