
export default class UserBlackForbidListDTO {
    public blacklists = new Array<string>(); // 黑名单列表
    public forbids = new Array<string>(); // 禁用用户列表
}
