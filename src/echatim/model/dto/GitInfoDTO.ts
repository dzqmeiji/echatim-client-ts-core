
export default class GitInfoDTO {
    public branch;
    public gitIdAbbrev;
    public gitTime;
    public authType;
    public modules;
    public toolkit;
    public einfo;
}
