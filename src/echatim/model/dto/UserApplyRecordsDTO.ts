
export default class UserApplyRecordsDTO {
    public id; // 申请记录id
    public attachText; // 附加文字

    public auid = ""; // 好友的auid
    public name = ""; // 好友名字
    public avatar = ""; // 好友头像
    public alias = ""; // 好友备注名
    public sign = ""; // 好友签名
    public createTime = new Date(1); // 创建时间
}
