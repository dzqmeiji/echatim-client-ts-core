
export default class SystemDocumentDTO {
    private id;
    private appKey;
    private category;
    private type;
    private content;
    private url;
    private createTime;
}
