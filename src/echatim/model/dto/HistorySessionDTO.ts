
export default class HistorySessionDTO {
    public id = 0; // 消息id
    public fromUser = ""; // fromUser auid
    public toTarget = ""; // toTarget auid
    public createTime = new Date(0); // 最后更新的时间撮
    public fromUserName = ""; // fromUser 用户名字
    public fromUserAvatar = ""; // fromUser 用户头像
    public toTargetName = ""; // toTarget 用户名字
    public toTargetAvatar = ""; // toTarget 用户头像
    public body:any = {}; // 消息内容
    public type = ""; // 消息类型
    public unread = 0; // 未读数
}
