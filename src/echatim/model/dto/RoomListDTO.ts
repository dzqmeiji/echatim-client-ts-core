
export default class RoomListDTO  {
    public rid = 0; // 群id
    public name = ""; //群名
    public owner = ""; // 群主,用户auid
    public announce = ""; // 群公告
    public introduce = ""; // 群简介
    public inviteText = ""; // 邀请文字
    public avatar = ""; // 群LOGO,头像
    public confJoinmode = ""; // 群加入模式
    public confBeinvite = ""; // 群员被邀请方式
    public confInviteother = ""; // 群员邀请权限(OWNER:仅群主;ALL:任何人也可以)
    public confUpdate = ""; // 群信息更新权限(OWNER:仅群主;ALL:任何人也可以)
    public maxMember = 0; // 群最大成员数量
    public status = ""; // 状态(NORMAL:正常;DEL: 已解散)
}
