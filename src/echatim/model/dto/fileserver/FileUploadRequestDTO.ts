

/**
 * FileUploadRequestDTO
 *
 * @author Daizhiqiang
 * @date 2019/11/22 18:07
 */

export default class FileUploadRequestDTO {
    public  signature = "";
    public  dir = "";
    public  host = "";
    public  expire = 0;
}


