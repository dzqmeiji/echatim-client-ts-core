const CryptoJS = require('crypto-js');

export default class Encrypt {
    public static encrypt(params:string, key:string):any{
        let content = '{}';
        try{
            const keyHex = CryptoJS.enc.Utf8.parse(key);
            const enc = CryptoJS.TripleDES.encrypt(params, keyHex, {
                mode: CryptoJS.mode.ECB,
                padding: CryptoJS.pad.Iso10126 //填充方式
            });
            content = enc.toString(); // 返回加密后的字符串
        }
        catch (e) {
            console.error(e);
        }
        return content;
    }

    public static decrypt(params:string, key:string):any{
        let content = '{}';
        try{
            const keyHex = CryptoJS.enc.Utf8.parse(key);
            const dec = CryptoJS.TripleDES.decrypt({
                ciphertext: CryptoJS.enc.Base64.parse(params)
            }, keyHex, {
                mode: CryptoJS.mode.ECB,
                padding: CryptoJS.pad.Iso10126 //
            });
            content =  dec.toString(CryptoJS.enc.Utf8); // 返回加密后的字符串
        }
        catch (e) {
            console.error(e);
        }
        return content;
    }
}
