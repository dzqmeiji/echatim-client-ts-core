/**
 * @author kong <androidsimu@163.com>
 * create by 2019/2/21 9:28
 **/
export class Topic {
    static prefix = 'topic.';
    static httpPrefix = '/v1/';

    static CONNECTION = class {
        static topic_name = Topic.prefix + "connection";
        static base_uri = Topic.httpPrefix + "connection";

        static METHOD = class {
            static AUTHORITY_REQUEST = "authority_request";
            static HEARTBEAT = "heartbeat";
        }
    };

    static APP_USER = class {
        static topic_name = Topic.prefix + "user";
        static base_uri = Topic.httpPrefix + "user";
        static METHOD = class {
            static ADD = "add";
            static UPDATE = "update";
            static LIST = "list";
            static GET = "get";
            static UPDATE_TOKEN = "update_token";
            static REFRESH_TOKEN = "refresh_token";
        }
    };

    // 通过服务器向客户端发送消息的prefix
    static APP_UPSTREAM_MESSAGE = class {
        static topic_name = Topic.prefix + "upstream_message";
        static base_uri = Topic.httpPrefix + "upstream_message";

        static METHOD = class {
            static SEND = "send";
            static BATCH_SEND = "batch_send";
            static SEND_ACK_TO_SERVER = "sendack_toserver";
            static CANCEL_MESSAGE = "cancel_message"; // 撤回消息
        }
    };


    // 服务器向客户端发送消息的prefix
    static APP_DOWNSTREAM_MESSAGE = class {
        static topic_name = Topic.prefix + "downstream_message";
        static base_uri = Topic.httpPrefix + "downstream_message";
        static METHOD = class {
            static SEND_MESSAGE_TO_CLIENT = "sendmessage_toclient";
            static SEND_EVENT_TO_CLIENT = "sendevent_toclient";
        }
    };


    static APP_CLIENT = class {
        static topic_name = Topic.prefix + "client";
        static base_uri = Topic.httpPrefix + "client";
        static METHOD = class {
            static USER_ONLINE = "user_online"; // 列出用户的在线状态
            static ADD_ONLINE_MONITOR = "add_online_monitor";// 设置用户的在线监听
            static REMOVE_ONLINE_MONITOR = "remove_online_monitor";// 移除用户的在线监听
        }
    };

    static APP_ROOM = class {
        static topic_name = Topic.prefix + "room";
        static base_uri = Topic.httpPrefix + "room";
        static METHOD = class {
            static ADD = "add"; // 添加群
            static UPDATE = "update"; // 更新群信息
            static LIST = "list"; // 列出群信息
            static DEL = "del"; // 删除群
            static GET = "get"; // 获取单个群信息
            static LIST_JOIN = "list_join"; // 列出当前用户加入的群
            static LIST_MEMBER = "list_member"; // 列出群信息/群成员
            static ADD_USER = "add_user"; // 添加群用户
            static DEL_USER = "del_user"; // 删除群用户
            static SEARCH = "search"; // 搜索群(通过name)
            static JOIN = "join"; // 加入群
            static EXIT = "exit"; // 退出群
            static MUTED = "muted"; // 设置群全员禁言
            static ADD_ROOM_ADMIN = "add_room_admin"; // 添加群管理员
            static REMOVE_ROOM_ADMIN = "remove_room_admin"; // 移除群管理员
            static LIST_ROOM_ADMIN = "list_room_admin"; // 群管理员列表
            static LIST_ROOM_MANAGED = "list_room_managed"; // 获取当前用户管理的群列表
            static CANCEL_ROOM_MESSAGE = "cancel_room_message"; // 移除群成员的消息

            static APPLY_RECORD = "apply_record";// 显示自己的入群申请记录
            static ADMIN_APPLY_RECORD = "admin_apply_record";// 群管理员显示用户入群申请记录
            static AGREE_ROOM = "agree_room";// 同意用户加入群
            static REJECT_ROOM = "reject_room";// 拒绝用户加入群
        }
    };


    static APP_USER_RELATION = class {
        static topic_name = Topic.prefix + "user_relation";
        static base_uri = Topic.httpPrefix + "user_relation";
        static METHOD = class {
            static ADD_FRIEND = "add_friend"; // 添加好友
            static DEL_FRIEND = "del_friend"; // 删除好友
            static LIST_FRIENDS = "list_friends"; // 列出好友列表
            static GET_FRIEND = "get_friend"; // 获取好友
            static MODIFY_BLACKLIST_FORBID = "add_blacklist";// 添加/移除黑名单, 添加/移除黑禁言
            static LIST_BLACKLIST_FORBID = "list_blacklists"; // 列出黑名单, 禁言列表
            static MODIFY_ALIAS = "modify_alias";// 修改IM好友别名
            static APPLY_RECORD = "apply_record";// 显示陌生人好友申请记录
            static MY_APPLY_RECORD = "my_apply_record";// 显示我的好友申请记录
            static AGREE_FRIEND = "agree_friend";// 同意陌生人为好友
            static REJECT_FRIEND = "reject_friend";// 拒绝陌生人为好友
        }
    };

    static APP_SYSTEM = class {
        static topic_name = Topic.prefix + "system";
        static base_uri = Topic.httpPrefix + "system";
        static METHOD = class {
            static INFO = "info"; // 后台系统信息
        }
    };

    static APP_SYSTEM_DOCUMENT = class {
        static topic_name = Topic.prefix + "system_document";
        static base_uri = Topic.httpPrefix + "system_document";
        static METHOD = class {
            static LIST_DOCUMENT = "list_document"; // 系统文档
        }
    };

    // 通过服务器向客户端发送消息的prefix
    static APP_HISTORY_MESSAGE = class {
        static topic_name = Topic.prefix + "history";
        static base_uri = Topic.httpPrefix + "history";
        static METHOD = class {
            static LIST_MESSAGE = "list_message"; // 列出历史消息
            static LIST_SESSION = "list_session"; // 列出用户历史会话列表
            static LIST_ROOM_RECENTLY = "list_room_recently"; // 列出用户加入的群最新一条记录
        }
    }

}

// export const ALL_prefixS = [prefix_CONNECTION.name, prefix_APP_USER.name];


