/**
 * @author kong <androidsimu@163.com>
 * create by 2019/2/21 9:28
 **/
export class TopicProfessional {
    static prefix = 'topic_professional.';
    static httpPrefix = '/v1/';

    static APP_USER_AUTH = class {
        static topic_name = TopicProfessional.prefix + "user_auth";
        static base_uri = TopicProfessional.httpPrefix + "user_auth";

        static METHOD = class {
            static REGISTER = "register";
            static LOGIN = "login";
            static LOGOUT = "logout";
            static INFO = "info";
        }
    };

    static APP_PUSH = class {
        static topic_name = TopicProfessional.prefix + "push";
        static base_uri = TopicProfessional.httpPrefix + "push";
        static METHOD = class {
            static SET = "set";
        }
    };

}

// export const ALL_prefixS = [prefix_CONNECTION.name, prefix_APP_USER.name];


