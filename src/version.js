const version = (function () {
    function getFullVersion() {
        const version = process.env.GIT_BRANCH.indexOf('feature_') >= 0 ? process.env.GIT_BRANCH.split('feature_')[1] : 'v0.00';
        const id = process.env.GIT_COMMITHASH.substring(process.env.GIT_COMMITHASH.length-5);
        return `${version}-${id}`;
    }
    return {
        version: getFullVersion(),
        git_commit_id: process.env.GIT_COMMITHASH,
        git_branch: process.env.GIT_BRANCH,
        platform: process.env.PLATFORM,
    }
})();

export default version;
